<?php

namespace App\Http\Controllers\Orders;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Miyama\Order;
use Miyama\Address;
use Miyama\Admin\Client;
use App\Http\Requests\GenerateOrderRequest;

class GenerateOrderController extends Controller
{

	public function create(Client $client)
	{
		return view('orders.create', compact('client'));
	}

    public function generate(GenerateOrderRequest $request, Client $client)
    {
        $order = $client->orders()->create($request->all());
        
        $this->isDelivery($request, $order);
        
        \Alert::info('La orden fue generada exitosamente');

        return redirect()->route('orders.index');
    }

    protected function isDelivery($request, $order)
    {

        if ($request->domicilio == true) {
            $order->update(['type' => 'delivery']);

            if ($request->type_address == 0) {
                $address = Address::create($request->all());
                $this->attachDelivery($order, $address->id);
            }else{
                $this->attachDelivery($order, $request->type_address);                
            }            
        }
    }
    
    protected function attachDelivery($order, $request)
    {
        $order->addresses()->attach($request);
    }
}
