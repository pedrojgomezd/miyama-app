<?php

namespace App\Http\Controllers\Orders;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Miyama\Order;
use Miyama\Orders\Statu;

class StatusOrderController extends Controller
{
	protected $status;

	function __construct(Statu $status)
	{
		$this->status = $status;
	}

	public function __invoke(Request $request, Order $order)
	{
    	$this->changeStatu($order);

    	\Alert::info("Su orden a cambiado a {$request->get('statu')}");

    	return redirect()->route('orders.show',['id' => $order->id]);
	}

	public function changeStatu($order)
	{
		return $this->getStatu()->each( function ($statu, $id) use ($order){
			$order->status()->attach($id);
		});
	}

    public function existStatu()
    {
		return $this->getStatu()->contains(request('statu'));
    }

    public function getStatu()
    {
    	return $this->status
					->pluck('slug', 'id');
    }
}
