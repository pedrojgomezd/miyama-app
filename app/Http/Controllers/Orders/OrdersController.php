<?php

namespace App\Http\Controllers\Orders;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Miyama\Order;
use App\QueryFilters\OrderFilters;

class OrdersController extends Controller
{

    public function index(OrderFilters $filters)
    {

    	$orders = Order::With('client', 'brand', 'product', 'status')
                        ->filter($filters)
                        ->latest('id')
                        ->paginate()
                        ->appends(request()->query());

        return view('orders.index', compact('orders'));
    }

    public function show($order)
    {
    	$order = Order::with('client', 'addresses', 'product', 'brand', 'status')
                        ->find($order);

    	return view('orders.show', compact('order'));
    }
}
