<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Miyama\Address;
use Miyama\Roles\Role;
use App\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('role')->get();
        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'role_id'          => 'required',
            'identification' => 'required|unique:users',
            'name'     => 'required',
            'email'         => 'required|unique:users',
            'mobil'         => 'required',
            'state_id'      => 'required',
            'city_id'       => 'required',
            'address'   => 'required',
        ]);

        $user = User::create([
            'role_id'           => $request['role_id'],
            'identification' => $request['identification'],
            'name'      => $request['name'],
            'email'          => $request['email'],
            'mobil'          => $request['mobil'],
            'phone'          => $request['phone'],
            'password'       => bcrypt('secret'),
        ]);
        $address = Address::make($request->all());

        $user->addresses()->save($address);

        return redirect()->route('users.show', ['id' => $user->id]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', compact('user'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request, [
            'role_id'          => 'required',
            'identification' => 'required|unique:users,identification,'.$user->id,
            'name'          => 'required',
            'email'         => 'required|unique:users,email,'.$user->id,
            'mobil'         => 'required',
            'state_id'      => 'required',
            'city_id'       => 'required',
            'address'   => 'required',
        ]);

        $user->update($request->all());

        $user->addresses()->first()->update($request->all());

       return redirect()->route('users.show', ['id' => $user->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
