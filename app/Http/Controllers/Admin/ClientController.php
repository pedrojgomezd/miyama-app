<?php

namespace App\Http\Controllers\Admin;

use Miyama\Address;
use Illuminate\Http\Request;
use Miyama\Admin\Client;
use App\QueryFilters\ClientFilters;
use App\Http\Controllers\Controller;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ClientFilters $filters)
    {
        $clients = Client::orderBy('id', 'DESC')
            ->with('addresses', 'addresses.state', 'addresses.city')
            ->filter($filters)
            ->paginate();
        
        return view('admin.clients.index',compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'type_identification' => 'required',
            'identification' => 'required',
            'business_name' => 'required',
            'mobil' => 'required',
            'email' => 'required',
        ]);

        $client = Client::create($request->all());

        //$client->addresses()->create($request->all());
        
        return redirect()->route('clients.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($client)
    {   
        $client = Client::with('orders', 'orders.status', 'orders.product')
                        ->find($client);

        return view('admin.clients.show', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('admin.clients.edit', compact('client'));        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Client $client)
    {
        $client->update($request->all());
        $client->addresses()->first()->update($request->all());

        \Alert::info('El cliente fue editado correctamente.');
        
        return redirect()->route('clients.show', ['id' => $client->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
