<?php

namespace App\Http\Controllers\Admin\Menus;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MenuController extends Controller
{
    public function index()
    {
    	return view('menus.index');
    }
}
