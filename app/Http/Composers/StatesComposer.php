<?php 

namespace App\Http\Composers;

use Illuminate\View\View;
use Miyama\Address\State;
/**
* Stados en la vistas
*/
class StatesComposer
{
	
	function compose(View $view)
	{
		$collection = collect(State::all());

        $states = $collection->pluck('name', 'id');

		$view->with('states', $states->all());
	}
}