<?php 

namespace App\Http\Composers;

use Illuminate\View\View;
use Miyama\Orders\Statu;
/**
* Status Composer
*/
class StatusComposer
{
	
	function compose(View $view)
	{
		$status = Statu::all()->pluck('name', 'id');
		$view->withStatus($status->toArray());
	}
}