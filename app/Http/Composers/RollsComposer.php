<?php 

namespace App\Http\Composers;

use Illuminate\View\View;
use Miyama\Roles\Role;

/**
* Rolle User Create
*/
class RollsComposer
{
	
	public function compose(View $view)
	{
		$rolls = Role::all();
		
		$view->with('rolls', 
			$rolls->pluck('name', 'id')
			->all()
		);
	}

}