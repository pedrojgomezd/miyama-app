<?php 

namespace App\Http\Composers;

use Illuminate\View\View;
/**
* Stados en la vistas
*/
class MenuComposer
{
	
	function compose(View $view)
	{
		$menu = [
			'clients' => [ 'icon' => 'si si-users',
				'submenu' => [
								'list' => ['route' => 'clients.index', 'icon' => 'fa fa-circle-o'],
								'create' => ['route' => 'clients.create', 'icon' => 'fa fa-circle-o'],
							],
			],
			'orders' 	=> ['route' => 'orders.index', 'icon' => 'si si-wrench'],
			'users' 	=> ['route' => 'users.index', 'icon' => 'si si-wrench'],
			'roles' 	=> ['route' => 'roles.index', 'icon' => 'si si-lock'],
			'crud' 		=> ['route' => 'lines.index', 'icon' => 'si si-wrench'],
			'menus' 	=> ['route' => 'menus.index', 'icon' => 'fa fa-list-alt'],
		];

		$view->with('menu', $menu);
	}
}
