<?php 

namespace App\Http\Composers;

use Illuminate\View\View;
use Miyama\Products\Brand;
use Miyama\Products\Line;
use App\User;
/**
* Stados en la vistas
*/
class OrdersCreateComposer
{
	
	function compose(View $view)
	{


		$view->with('brands', $this->brands())
			->with('lines', $this->lines())
			->with('techs',$this->techs());
	}

	public function lines()
	{		
		return Line::all()->pluck('name', 'id')->all();
	}

	public function brands()
	{
		return Brand::all()->pluck('name', 'id')->all();
	}

	public function techs()
	{
		return User::techs()->pluck('name', 'id')->all();
	}
}