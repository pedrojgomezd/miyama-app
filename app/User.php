<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\Addressable;

class User extends Authenticatable
{
    use Notifiable, Addressable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'identification', 'role_id', 'phone', 'mobil','email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function orders()
    {
        return $this->hasMany(\Miyama\Order::class);
    }

    public function role()
    {
        return $this->belongsTo(\Miyama\Roles\Role::class);
    }

    public function hasRole($permission)
    {
        if (is_string($permission)) {
            return $this->role->permissions->pluck('name')->intersect($permission)->count();
        }
    }

    public function scopeTechs($query)
    {
        return $this->whereHas('role', function($query){
            $query->where('name', 'tech');
        })
        ->get();
    }
}
