<?php  

namespace App\Traits;

use App\QueryFilters\QueryFilter;

/**
* Trait para filtrar lo que sea.
*/
trait Filtrable
{
    public function scopeFilter($query, QueryFilter $filters)
    {
        return $filters->apply($query);
    }
}