<?php 

namespace App\Traits;

use Miyama\Address;

trait Addressable
{
    public function addresses()
    {
        return $this->morphToMany(Address::class, 'addressable');
    }

    public function getFullAddressAttribute()
    {
        if ($address = $this->addresses->first()) 
        {
    		return $address->address. ' <strong>('.$address->city->name.'-'.$address->state->name.')</strong>';
        }
        return 'Actualmente no posee direccion, debe actualizar.';

    }
}
