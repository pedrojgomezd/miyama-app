<?php 

namespace App\Observers;

use Miyama\Order;

/**
* Observe of Orders
*/
class OrderObserver
{

	public function creating(Order $order)
	{
		$order->user_id = auth()->id() ?? 1;
	}

	public function created(Order $order)
	{
		$order->status()->attach(1);		
	}

}
