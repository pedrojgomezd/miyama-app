<?php

namespace Miyama;

use App\User;
use App\Models\Order;
use App\Models\Admin\Client;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = ['state_id', 'city_id', 'address'];

    public function users()
    {
        return $this->morphedByMany(User::class, 'addressables');
    }

    public function clients()
    {
        return $this->morphedByMany(Client::class, 'addressable');
    }

    public function orders()
    {
    	return $this->morphedByMany(Order::class, 'addressable');
    }

    public function state()
    {
    	return $this->belongsTo(Address\State::class);
    }

    public function city()
    {
    	return $this->belongsTo(Address\City::class);
    }
}
