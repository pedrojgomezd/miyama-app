<?php

namespace Miyama\Roles;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name', 'label'];

    protected static function boot()
    {
    	parent::boot();

    	static::creating(function($model){
    		$model->attributes['label'] = str_slug($model->name);
    	});
    }

    public function permissions()
    {
    	return $this->belongsToMany(Permission::class);
    }

    public function users()
    {
    	return $this->hasMany(\App\User::class);
    }
}
