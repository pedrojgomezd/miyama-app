<?php

namespace Miyama\Menus;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    protected $fillable = ['menu_id', 'title', 'route', 'target', 'icon', 'parent_id', 'order'];

    
}
