<?php

namespace Miyama\Products;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
	protected $fillable = ['name', 'sub_line_id'];

    public function sub_line()
    {
    	return $this->belongsTo(SubLine::class);
    }
    
}
