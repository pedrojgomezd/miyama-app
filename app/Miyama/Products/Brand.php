<?php

namespace Miyama\Products;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = ['name', 'slug'];

    public function setSlugAttribute($value)
    {
    	$this->attributes['slug'] = str_slug($value);
    }
}
