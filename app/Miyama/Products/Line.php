<?php

namespace Miyama\Products;

use Illuminate\Database\Eloquent\Model;

class Line extends Model
{
    protected $fillable = ['name'];

    public function sub_lines()
    {
    	return $this->hasMany(SubLine::class);
    }

    public function products()
    {
    	return $this->hasManyThrough(Product::class, SubLine::class);
    }
}
