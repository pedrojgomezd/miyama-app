<?php

namespace Miyama\Products;

use Illuminate\Database\Eloquent\Model;

class SubLine extends Model
{
    protected $fillable = ['name', 'line_id'];

    public function line()
    {
    	return $this->belongsTo(Line::class);
    }

    public function products()
    {
    	return $this->hasMany(Product::class);
    }
}
