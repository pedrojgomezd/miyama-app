<?php

namespace Miyama;

use Illuminate\Database\Eloquent\Model;
use App\TimeLine;
use App\Traits\{Addressable, Filtrable};

class Order extends Model
{
    use Addressable, Filtrable;

    protected $fillable = ['client_id', 'user_id', 'tech_id', 'brand_id', 'product_id', 'serial', 'model','fail', 'type'];

    /*
        Relations
     */
    public function client()
    {
    	return $this->belongsTo(Admin\Client::class);
    }

    public function user()
    {
    	return $this->belongsTo(\App\User::class);
    }

    public function tech()
    {
        return $this->belongsTo(\App\User::class, 'tech_id');
    }

    public function brand()
    {
    	return $this->belongsTo(Products\Brand::class);
    }

    public function product()
    {
    	return $this->belongsTo(Products\Product::class);
    }

    public function status()
    {
        return $this->belongsToMany(Orders\Statu::class)
                ->withTimestamps()
                ->latest('id');
    }

    public function timeLines()
    {
        return $this->hasMany(TimeLine::class);
    }


    /*
        Attributes
     */
    public function getStatuAttribute()
    {
        $statu = $this->status->last();
        return "<span class='label {$statu->class}'><i class='{$statu->icon}'></i> {$statu->name}</span>";
    }

}
