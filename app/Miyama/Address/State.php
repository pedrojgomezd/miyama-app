<?php

namespace Miyama\Address;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $fillable = ['name', 'iso_3166-2'];

    public function cities()
    {
    	return $this->hasMany(City::class);
    }

    public function getCitiesNowAttribute()
    {
    	$collection = collect($this->cities);

        $cities = $collection->pluck('name', 'id');

		return $cities->toArray();
    }
}
