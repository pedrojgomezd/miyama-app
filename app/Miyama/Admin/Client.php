<?php

namespace Miyama\Admin;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Addressable;
use App\Traits\Filtrable;


class Client extends Model
{
    use Addressable, Filtrable;

    protected $fillable = ['type_identification','identification', 'business_name', 'mobil', 'phone', 'email'];

    public function orders()
    {
        return $this->hasMany(\Miyama\Order::class);
    }

    public function getFullIdentificationAttribute()
    {
    	return $this->type_identification.'-'.$this->identification;
    }

}