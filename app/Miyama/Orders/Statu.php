<?php

namespace Miyama\Orders;

use Illuminate\Database\Eloquent\Model;
use Miyama\Order;

class Statu extends Model
{
    protected $fillable = ['name', 'slug', 'icon', 'class', 'statu'];

    protected $touches = ['orders'];

    public function orders()
    {
    	return $this->belongsToMany(Order::calss);
    }
}
