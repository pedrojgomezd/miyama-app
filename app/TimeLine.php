<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Miyama\Order;

class TimeLine extends Model
{
    protected $fillable = ['user_id', 'order_id', 'body'];

    public function order()
    {
    	return $this->belongsTo(Order::class);
    }
}
