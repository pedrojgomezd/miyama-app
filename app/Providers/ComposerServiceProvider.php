<?php

namespace App\Providers;

use App\Http\Composers\MenuComposer;
use App\Http\Composers\OrdersCreateComposer;
use App\Http\Composers\RollsComposer;
use App\Http\Composers\StatesComposer;
use App\Http\Composers\StatusComposer;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;


class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        \Carbon\Carbon::setLocale(config('app.locale'));
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        View::composer('layouts.menu', MenuComposer::class);

        View::composer(['layouts.form.address-inputs', 
                        'admin.clients.edit',
                        'admin.users.edit', 
                        'orders.create'], 
            StatesComposer::class);
        
        View::composer(['orders.create', 
                        'orders.index'], 
            OrdersCreateComposer::class);

        View::composer('admin.users.create', RollsComposer::class);

        View::composer('orders.block.filters-modal', StatusComposer::class);
    }
}
