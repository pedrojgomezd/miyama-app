<?php

namespace App\Providers;

use Miyama\Order;
use App\Observers\OrderObserver;
use Laravel\Dusk\DuskServiceProvider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Schema::defaultStringLength(191);

        Order::observe(OrderObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
         if ($this->app->environment('local', 'testing')) {
                $this->app->register(DuskServiceProvider::class);
            }
    }
}
