<?php

namespace App\QueryFilters;

/**
* FIltros para ordenes
*/
class OrderFilters extends QueryFilter
{
	protected $filters = ['order', 'brand', 'client'];
	
	public function order($type = 'DESC')
	{
		return $this->builder->orderBy('id', $type);
	}

	public function brand($id = '')
	{
		return $this->builder->where('brand_id', $id);
	}

	public function client($name)
	{
		return $this->builder->whereHas('client', function ($query) use ($name) {
			$query->where('business_name', 'like', '%'.$name.'%');
		});
	}
}