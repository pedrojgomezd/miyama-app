<?php

namespace App\QueryFilters;

/**
* FIltros para ordenes
*/
class ClientFilters extends QueryFilter
{
	
	public function order($type = 'DESC')
	{
		return $this->builder->orderBy('id', $type);
	}

	public function business_name($business_name = '')
	{
		return $this->builder->where('business_name', 'LIKE', '%'.$business_name.'%');
	}
}