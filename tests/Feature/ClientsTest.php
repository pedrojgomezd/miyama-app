<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClientsTest extends TestCase
{
    use DatabaseTransactions;

    public function setUp()
    {
    	parent::setUp();
    	$user = create(\App\User::class, ['type' => 'admin']);
    	$this->signIn($user);
    }

    /** @test */
    public function only_user_type_admin_or_can_create_a_client ()
    {
    	$this->get('/reception/clients/create')
    		->assertSee('Nuevo Cliente');

    	$client = make(\Miyama\Admin\Client::class);

    	$address = make(\Miyama\Address::class);

    	$this->post('/reception/clients', $client->toArray())
    		->assertRedirect(route('clients.index'));
    }
}
