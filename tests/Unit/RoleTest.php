<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RoleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_a_role_has_label_type_slug()
    {
        $role = create(\Miyama\Roles\Role::class, [
        	'name' => 'admin Complete'
        ]);

        $this->assertEquals('admin-complete', $role->label);
    }
}
