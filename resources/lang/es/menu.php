<?php 
return [
	'clients' 	=> 'Clientes',
	'orders'  	=> 'Ordenes',
	'users'   	=> 'Usuarios',
	'list'   	=> 'Listar',
	'create'   	=> 'Crear',
];