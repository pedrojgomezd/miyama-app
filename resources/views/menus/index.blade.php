@extends('layouts.template')

@section('page-heading')
<i class="fa fa-list-alt"></i> Menus <a href="" class="btn btn-success btn-sm"><i class="si si-plus"></i> Nuevo Menu</a>
@endsection

@section('content')

<div class="block">
	<div class="block-header bg-primary">
		<div class="block-title">
			Lista de Menus
		</div>
	</div>
	<div>
		<table class="table">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Admin</td>
					<td>
						<div class="btn-group">
	                        <div class="btn-group">	                            
	                            <a href="" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Construir</a>
	                            <a href="" class="btn btn-sm btn-danger"><i class="si si-trash"></i></a>
	                        </div>
	                    </div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>


@endsection