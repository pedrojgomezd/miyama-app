<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Post</title>
	<link rel="stylesheet" href="/css/app.css">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				@foreach($posts as $post)

					<div class="panel panel-default">
						<div class="panel-heading">
							<h4>{{ $post->title }}</h4>
						</div>
						<div class="panel-body">
							{{ $post->body }}
						</div>
					</div>

				@endforeach
			</div>
		</div>
	</div>
</body>
</html>