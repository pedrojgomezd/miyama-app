@extends('layouts.template')

@section('page-heading')
<i class="si si-users"></i> Clientes 
@endsection

@section('content')
<div class="block block-themed block-rounded">
	<div class="block-header bg-primary">
		<div class="block-options-simple btn-group btn-group-xs">
			<a href="{{ route('clients.create') }}" class="btn btn-default"><i class="si si-plus"></i> Crear</a>
		</div>
		<h3 class="block-title">Lista de Clientes</h3>
	</div>
	<div class="">
	<table class="table table-hover">
		<thead>
			<tr>
				<th>Identificacion</th>
				<th>Razon Social</th>
				<th>Email</th>
				<th>Telefonos</th>
				<th>Accion</th>
			</tr>
		</thead>
		<tbody>
			@foreach($clients as $client)
				<tr>
					<td>{{ $client->full_identification }}</td>
					<td>{{ $client->business_name }}</td>
					<td>{{ $client->email }}</td>
					<td>
						{{ $client->mobil }} /
						{{ $client->phone }}
					</td>
					<td>
						<div class="btn-group">
	                        <div class="btn-group">	                            
	                            <a href="{{ route('clients.show', ['id'=> $client->id]) }}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title type="button" data-original-title="Ver clinete">
	                            	<i class="fa fa-eye"></i>
	                            </a>
	                            <a href="{{ route('clients.edit', ['id'=> $client->id]) }}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title type="button" data-original-title="Editar cliente">
	                            	<i class="fa fa-edit"></i>
	                            </a>
	                            <a href="{{ route('order.create', ['id'=> $client->id]) }}" class="btn btn-sm btn-default" data-toggle="tooltip" data-placement="top" title type="button" data-original-title="Generar nueva orden">
	                            	<i class="si si-wrench"></i>
	                            </a>
	                        </div>
	                    </div>
					</td>
				</tr>
			@endforeach			
		</tbody>
	</table>
	</div>
	{!! $clients->links() !!}
</div>
@endsection