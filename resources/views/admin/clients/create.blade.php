@extends('layouts.template')

@section('page-heading')
<i class="si si-users"></i> Nuevo Cliente
@endsection

@section('content')
<div class="block block-themed block-rounded">
	<div class="block-header bg-primary">
		<h3 class="block-title">Datos del Cliente</h3>
	</div>
	<div class="block-content">
	{{ Form::open(['mehtod'=>'POST', 'route' => 'clients.store']) }}
		<div class="row">
			<div class="col-md-2">
				{!! Field::select('type_identification', ['V'=>'V', 'J'=>'J', 'G'=>'G']) !!}
			</div>
			<div class="col-md-3">
				{!! Field::text('identification', ['placeholder' => '19747349']) !!}
			</div>
			<div class="col-md-7">
				{!! Field::text('business_name', ['placeholder' => 'Jose Luis']) !!}					
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				{!! Field::text('email', ['placeholder' => 'ejemplo@gmail.com']) !!}
			</div>
			<div class="col-md-4">
				{!! Field::text('phone', ['placeholder' => '02642617878']) !!}
			</div>
			<div class="col-md-4">
				{!! Field::text('mobil', ['placeholder' => '04149023212']) !!}
			</div>
		</div>

		@include('layouts.form.address-inputs')

		<div class="form-group">
			<button class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
			<a href="/admin" class="btn btn-default"><i class="fa fa-"></i> Salir</a>
		</div>
	{{ Form::close() }}
	</div>
</div>
@endsection