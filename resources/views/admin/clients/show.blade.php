@extends('layouts.template')

@section('page-heading')
<i class="icon-users"></i> Cliente
@endsection

@section('content')

<div class="block block-themed block-rounded">
	<div class="block-header bg-primary">
		<div class="block-options-simple btn-group btn-group-xs">
			<a href="{{ route('clients.edit', ['id'=> $client->id]) }}" 
				class="btn btn-sm btn-default" 
				data-toggle="tooltip" 
				data-placement="top" 
				title type="button" 
				data-original-title="Editar cliente">
        		<i class="fa fa-edit"></i>
        	</a>
        	
        	<a href="{{ route('order.create', ['id'=> $client->id]) }}" 
        		class="btn btn-sm btn-default" 
        		data-toggle="tooltip" 
        		data-placement="top" 
        		title type="button" 
        		data-original-title="Generar nueva orden">
        		<i class="si si-wrench"></i>
	        </a>
		</div>
		<h3 class="block-title">Datos de Cliente</h3>
	</div>
	<div class="">
		<table class="table table-condensed">
			<tbody>
				<tr>
					<th>Identificacion</th>
					<th>Razon Social</th>
					<th>Email</th>
				</tr>
				<tr>
					<td>{{ $client->full_identification }}</td>
					<td>{{ $client->business_name }}</td>
					<td>{{ $client->email }}</td>
				</tr>
				<tr>
					<th>Telefonos</th>
					<th colspan="2">Direccion</th>
				</tr>
				<tr>
					<td>{{ $client->mobil }} / {{ $client->phone }}</td>
					<td colspan="2">{!! $client->full_address !!}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="block block-themed block-rounded">
	<div class="block-header bg-info">
		<h3 class="block-title"><small><i class="si si-wrench"></i></small> Ordenes</h3>
	</div>
	<div class="">
		<table class="table table-condensed ">
			<thead>
				<tr>
					<th>#</th>
					<th>Articulo</th>
					<th>Serial</th>
					<th>Tecnico</th>
					<th>Estatus</th>
					<th>Accion</th>
				</tr>
			</thead>
			<tbody>
			@foreach($client->orders as $order)
				<tr>
					<td>{{ $order->id }}</td>
					<td>{{ $order->product->name }}</td>
					<td>{{ $order->serial }}</td>
					<td>{{ $order->tech_id }}</td>
					<td>{!! $order->statu !!}</td>
					<td><span class="label bg-primary">Revision</span></td>
					<td>
						<a href="{{ route('orders.show', ['id'=> $client->id]) }}" class="btn btn-success btn-sm"><i class="fa fa-eye"></i></a>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>

@endsection