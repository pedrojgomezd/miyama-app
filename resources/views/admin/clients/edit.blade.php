@extends('layouts.template')

@section('page-heading')
<i class="si si-users"></i> Editar Clientes 
@endsection

@section('content')
<div class="block block-themed block-rounded">
	<div class="block-header bg-primary">
		<h3 class="block-title">Datos del Cliente</h3>
	</div>
	<div class="block-content">
	{{ Form::open(['method'=>'PUT', 'url' => route('clients.update', ['id' => $client->id])]) }}
		<div class="row">
			<div class="col-md-2">
				{!! Field::select('type_identification', ['V'=>'V','J'=>'J','G'=>'G'], $client->type_identification) !!}
			</div>
			<div class="col-md-3">
				{!! Field::text('identification', $client->identification,['placeholder' => '19747349']) !!}
			</div>
			<div class="col-md-4">
				{!! Field::text('business_name', $client->business_name, ['placeholder' => 'Lujan Rodriguez']) !!}
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				{!! Field::text('email', $client->email, ['placeholder' => 'ejemplo@gmail.com']) !!}
			</div>
			<div class="col-md-4">
				{!! Field::text('phone', $client->phone, ['placeholder' => '02642617878']) !!}
			</div>
			<div class="col-md-4">
				{!! Field::text('mobil', $client->mobil,['placeholder' => '04149023212']) !!}
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				{!! Field::select('state_id', $states,  $client->addresses->first()->state->id) !!}
			</div>
			<div class="col-md-4">
				{!! Field::select('city_id', $client->addresses->first()->state->cities_now, $client->addresses->first()->city->id) !!}
			</div>
			<div class="col-md-4">
				{!! Field::text('address', $client->addresses->first()->address,['placeholder' => 'Urb Los Laureles Secto 8 #32']) !!}
			</div>
		</div>

		<div class="form-group">
			<button class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
			<button class="btn btn-default"><i class="fa fa-"></i> Salir</button>
		</div>
	{{ Form::close() }}
	</div>
</div>
@endsection