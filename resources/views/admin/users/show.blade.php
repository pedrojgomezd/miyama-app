@extends('layouts.template')

@section('page-heading')
<a href="{{ route('users.edit', ['id' => $user->id]) }}" class="btn btn-success"><i class="fa fa-edit"></i> Editar</a>
@endsection

@section('content')
<div class="block">
    <!-- Basic Info -->
    <div class="bg-image" style="background-image: url('{{ asset('storage/bg-cover.jpg') }}');">
        <div class="block-content bg-primary-dark-op text-center overflow-hidden">
            <div class="push-30-t push animated fadeInDown">
                <img class="img-avatar img-avatar96 img-avatar-thumb" src="{{ asset("storage/{$user->avatar}") }}" alt="">
            </div>
            <div class="push-30 animated fadeInUp">
                <h2 class="h4 font-w600 text-white push-5">{{ $user->name }}</h2>
                <h3 class="h5 text-gray">{{ __($user->role->name) }}</h3>
                <h3 class="h5 text-gray">{{ $user->mobil }}</h3>
            </div>
        </div>
    </div>
    <!-- END Basic Info -->

    <!-- Stats -->
    <div class="block-content text-center">
        <div class="row items-push text-uppercase">
            <div class="col-xs-6 col-sm-4">
                <div class="font-w700 text-gray-darker animated fadeIn">Ordenes</div>
                <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0)">80</a>
            </div>
            <div class="col-xs-6 col-sm-4">
                <div class="font-w700 text-gray-darker animated fadeIn">Facturas Pendientes</div>
                <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0)">16</a>
            </div>
            <div class="col-xs-6 col-sm-4">
                <div class="font-w700 text-gray-darker animated fadeIn">Facturas Vencidas</div>
                <a class="h2 font-w300 text-primary animated flipInX" href="javascript:void(0)">4</a>
            </div>
        </div>
    </div>
    <!-- END Stats -->
</div>

<div class="row">
    <div class="col-md-offset-4 col-sm-7 col-lg-8">
        <!-- Timeline -->
        <div class="block">
            <div class="block-header bg-gray-lighter">
                <ul class="block-options">
                    <li>
                        <button type="button" data-toggle="block-option" data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
                    </li>
                    <li>
                        <button type="button" data-toggle="block-option" data-action="refresh_toggle" data-action-mode="demo"><i class="si si-refresh"></i></button>
                    </li>
                </ul>
                <h3 class="block-title"><i class="fa fa-newspaper-o"></i> Actividad</h3>
            </div>
            <div class="block-content">
                <ul class="list list-timeline pull-t">
                    <!-- Facebook Notification -->
                    <li>
                        <div class="list-timeline-time">Hace 3 horas</div>
                        <i class="fa fa-file list-timeline-icon bg-primary-dark"></i>
                        <div class="list-timeline-content">
                            <p class="font-w600">Ingreso un Cliente</p>
                            <p class="font-s13"></p>
                        </div>
                    </li>
                    <!-- END Facebook Notification -->
                </ul>
            </div>
        </div>
        <!-- END Timeline -->
    </div>
</div>
@endsection