@extends('layouts.template')

@section('page-heading')
<i class="si si-user"></i> Nuevo Usuario 
@endsection

@section('content')
<div class="block block-themed block-rounded">
	<div class="block-header bg-primary">
		<h3 class="block-title">Datos del Usuario</h3>
	</div>
	<div class="block-content">
	<div class="alert alert-info">
		<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
		La constraseña por defecto en '<b>secret</b>', el usuario debe cambiarla para mayor seguridad.
	</div>
	{{ Form::open(['mehtod'=>'POST', 'route' => 'users.store']) }}
		<div class="row">
			<div class="col-md-2">
				{!! Field::select('role_id', $rolls) !!}
			</div>
			<div class="col-md-3">
				{!! Field::text('identification', ['placeholder' => '19747349']) !!}
			</div>
			<div class="col-md-7">
				{!! Field::text('name', ['placeholder' => 'Jose Luis']) !!}					
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				{!! Field::text('email', ['placeholder' => 'ejemplo@gmail.com']) !!}
			</div>
			<div class="col-md-4">
				{!! Field::text('phone', ['placeholder' => '02642617878']) !!}
			</div>
			<div class="col-md-4">
				{!! Field::text('mobil', ['placeholder' => '04149023212']) !!}
			</div>
		</div>

		@include('layouts.form.address-inputs')

		<div class="form-group">
			<button class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
			<button class="btn btn-default"><i class="fa fa-"></i> Salir</button>
		</div>
	{{ Form::close() }}
	</div>
</div>
@endsection