@extends('layouts.template')

@section('page-heading')
<i class="si si-users"></i> Usuarios 
@endsection

@section('content')
<div class="block block-themed block-rounded">
	<div class="block-header bg-primary">
		<div class="block-options-simple btn-group btn-group-xs">
			<a href="{{ route('users.create') }}" class="btn btn-default"><i class="si si-plus"></i> Crear</a>
		</div>
		<h3 class="block-title">Lista de Usuario</h3>
	</div>
	<div class="">
	<table class="table table-striped table-vcenter">
		<thead>
			<tr>
				<th class="text-center"><i class="si si-user"></i></th>
				<th>Nombre</th>
				<th>Email</th>
				<th>Telefonos</th>
				<th>Role</th>
				<th>Accion</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
			<tr>
	            <td class="text-center">
	                <img class="img-avatar img-avatar24" src="{{ asset("storage/{$user->avatar}") }}" alt="">
	            </td>
	            <td class="font-w600">{{ $user->name }}</td>
	            <td>{{ $user->email }}</td>
	            <td>{{ $user->mobil }}</td>
	            <td>{{ __($user->role->label) }}</td>
	            <td class="text-center">
	                <div class="btn-group">
	                    <a href="{{ route('users.edit', ['id' => $user->id]) }}" class="btn btn-xs btn-default"  data-toggle="tooltip" title="" data-original-title="Edit User"><i class="fa fa-pencil"></i></a>
	                    <a href="{{ route('users.show', ['id' => $user->id]) }}" class="btn btn-xs btn-default" data-toggle="tooltip" title="" data-original-title="Mostrar User"><i class="fa fa-eye"></i></a>
	                </div>
	            </td>
	        </tr>
	        @endforeach
		</tbody>
	</table>
	</div>
</div>
@endsection