@extends('layouts.template')

@section('page-heading')
<i class="si si-user"></i> Editar Usuario 
@endsection

@section('content')
<div class="block block-themed block-rounded">
	<div class="block-header bg-primary">
		<h3 class="block-title">Datos del Usuario</h3>
	</div>
	<div class="block-content">
	{{ Form::open(['method'=>'PUT', 'url' => route('users.update', ['id' => $user->id])]) }}
		<div class="row">
			<div class="col-md-2">
				{!! Field::select('role_id', ['1'=>'Administrador', '2'=>'Usuario', '3'=>'Tecnico', 4 => 'Recepcion'], $user->role->id) !!}
			</div>
			<div class="col-md-3">
				{!! Field::text('identification', $user->identification) !!}
			</div>
			<div class="col-md-7">
				{!! Field::text('name', $user->name) !!}					
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				{!! Field::text('email', $user->email) !!}
			</div>
			<div class="col-md-4">
				{!! Field::text('phone', $user->phone) !!}
			</div>
			<div class="col-md-4">
				{!! Field::text('mobil', $user->mobil) !!}
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				{!! Field::select('state_id', $states,  $user->addresses->first()->state->id) !!}
			</div>
			<div class="col-md-4">
				{!! Field::select('city_id', $user->addresses->first()->state->cities_now, $user->addresses->first()->city->id) !!}
			</div>
			<div class="col-md-4">
				{!! Field::text('address', $user->addresses->first()->address,['placeholder' => 'Urb Los Laureles Secto 8 #32']) !!}
			</div>
		</div>

		<div class="form-group">
			<button class="btn btn-primary"><i class="fa fa-save"></i> Guardar</button>
			<button class="btn btn-default"><i class="fa fa-"></i> Salir</button>
		</div>
	{{ Form::close() }}
	</div>
</div>
@endsection