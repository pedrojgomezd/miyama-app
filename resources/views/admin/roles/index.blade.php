@extends('layouts.template')

@section('page-heading')
<i class="si si-lock"></i> Roles <a class="btn btn-success btn-sm" data-toggle="modal" href='#new-role'><i class="si si-plus"></i> Nuevo Role</a>
@endsection

@section('content')
{!! Alert::render() !!}
<div class="block block-rounded block-themed">
	<div class="block-header bg-primary">
		<h3 class="block-title">Lista de Roles</h3>
	</div>
	<div>
		<table class="table">
			<thead>
				<tr>
					<th>Nombre</th>
					<th>Nombre Visible</th>
					<th>Acciones</th>
				</tr>
			</thead>
			<tbody>
			@foreach($roles as $role)
				<tr>
					<td>{{ $role->name }}</td>
					<td>{{ $role->label }}</td>
					<td>
						<a href="" class="btn btn-info btn-sm"><i class="fa fa-eye"></i> Ver</a>
						<a href="{{ route('roles.edit', ['id' => $role->id]) }}" class="btn btn-success btn-sm"><i class="fa fa-edit"></i> Editar</a>
					</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</div>
</div>
<div class="modal fade" id="new-role">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
				<h4 class="modal-title">Nuevo Role en el Sistema</h4>
			</div>
			{{ Form::open(['method' => 'POST', 'route' => 'roles.store']) }}
			<div class="modal-body">
				<div class="row">
					<div class="col-md-6">{!! Field::text('name', ['pl' => 'admin']) !!}</div>
					<div class="col-md-6">{!! Field::text('label', ['pl' => 'Aministrador']) !!}</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
				<button type="submit" class="btn btn-primary">Guardar</button>
			</div>
			{{ Form::close() }}
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection