@extends('layouts.template')

@section('page-heading')
<i class="si si-lock"></i> Editar Roles 
@endsection

@section('content')
<div class="block block-rounded block-bordered">
	<div class="block-header ">
		<h3 class="block-title">Editar Role</h3>
	</div>
	<div class="block-content">
	{{ Form::open(['method' => 'PUT', 'url' => route('roles.update', ['id' => $role->id])]) }}
		<div class="row">
			<div class="col-md-6">
				{!! Field::text('name', $role->name) !!}
			</div>
			<div class="col-md-6">
				{!! Field::text('label', $role->label) !!}
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				{!! Field::checkboxes('permissions', $permissions, $permissionsOfRole) !!}
			</div>
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-primary">Guardar</button>
		</div>
	{{ Form::close() }}
	</div>
</div>
@endsection