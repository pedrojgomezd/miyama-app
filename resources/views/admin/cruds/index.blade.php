@extends('layouts.template')

@section('page-heading')
<i class="si si-users"></i> Lineas y Sub-Lineas <a class="btn btn-success btn-sm" data-toggle="modal" href='#modal-id'>Agregar Lines</a>
@endsection

@section('content')
@foreach($lines as $line)

<div class="col-md-6">
	<div class="block block-themed block-rounded">
		<div class="block-header bg-primary">
			<div class="block-options-simple btn-group btn-group-xs">
				<a href="{{ route('clients.create') }}" class="btn btn-default"><i class="si si-plus"></i> Nueva Sub Linea</a>
			</div>
			<h3 class="block-title">Liena {{ $line->name }}</h3>
		</div>
		<div class="">
			<table class="table">
				<thead>
					<tr>
						<th>Sub Linea</th>
						<th>Productos</th>
					</tr>
				</thead>
				<tbody>
				@foreach($line->sub_lines as $sub_line)
					<tr>
						<td>{{ $sub_line->name }}</td>
						<td>
							<ul>
								@foreach($sub_line->products as $product)
								<li>{{ $product->name }}</li>
								@endforeach
							</ul>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endforeach
<div class="modal fade" id="modal-id">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Nueva Linea</h4>
			</div>
			{{ Form::open() }}
			<div class="modal-body">
					{!! Field::text('line_name') !!}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary">Guardar</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>
@endsection