@extends('layouts.template')

@section('page-heading-img')
<div class="bg-image overflow-hidden" style="background-image: url('{{ asset('storage/bg-dashboard.jpg') }}');">
    <div class="bg-black-op">
        <div class="content content-narrow">
            <div class="block block-transparent">
                <div class="block-content block-content-full">
                    <h1 class="h1 font-w300 text-white animated fadeInDown push-50-t push-5">Dashboard</h1>
                    <h2 class="h4 font-w300 text-white-op animated fadeInUp">Bienvenido {{ auth()->user()->name}}</h2>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')

<div class="row text-uppercase">
    <div class="col-xs-6 col-sm-3">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="text-muted">
                    <small><i class="si si-calendar"></i> Este mes</small>
                </div>
                <div class="font-s12 font-w700">Nuevos Clientes</div>
                <a class="h2 font-w300 text-primary" href="base_comp_charts.html" data-toggle="countTo" data-to="83">83</a>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="text-muted">
                    <small><i class="si si-calendar"></i> Este mes</small>
                </div>
                <div class="font-s12 font-w700">Ordenes Nuevas</div>
                <a class="h2 font-w300 text-primary" href="base_comp_charts.html" data-toggle="countTo" data-to="120">120</a>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="text-muted">
                    <small><i class="si si-calendar"></i> Este mes</small>
                </div>
                <div class="font-s12 font-w700">Ordenes Pendientes</div>
                <a class="h2 font-w300 text-primary" href="base_comp_charts.html" data-toggle="countTo" data-to="32" >32</a>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3">
        <div class="block block-rounded">
            <div class="block-content block-content-full">
                <div class="text-muted">
                    <small><i class="si si-calendar"></i> Este mes</small>
                </div>
                <div class="font-s12 font-w700">Facturado</div>
                <a class="h2 font-w300 text-primary" href="base_comp_charts.html" data-toggle="countTo" data-to="2450" data-before="$">$2450</a>
            </div>
        </div>
    </div>
</div>

@endsection