<div class="row">
	<div class="col-md-4">
		{!! Field::select('state_id', $states, ['class'=>'js-select2']) !!}
	</div>
	<div class="col-md-4">
		{!! Field::select('city_id', null, ['class'=>'js-select2']) !!}
	</div>
	<div class="col-md-4">
		{!! Field::text('address', ['placeholder' => 'Urb Los Laureles Secto 8 #32']) !!}
	</div>
</div>