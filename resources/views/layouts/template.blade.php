
<!DOCTYPE html>
<!--[if IE 9]>         <html class="ie9 no-focus" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> 
<html class="no-focus" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>{{ config('app.name') }}</title>

        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- END Icons -->
         <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Stylesheets -->
        <!-- Web fonts -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400italic,600,700%7COpen+Sans:300,400,400italic,600,700">
        <!-- Bootstrap and Theme CSS framework -->
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <link rel="stylesheet" href="{{ mix('css/main.css') }}">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>
    <div id="app"> 
        <div id="page-container" class="sidebar-l sidebar-o side-scroll header-navbar-fixed">

            @include('layouts.aside')

            @include('layouts.menu')

            @include('layouts.nav')

            <main id="main-container">
                <!-- Page Header -->
                @yield('page-heading-img')
                <div class="content bg-gray-lighter">
                    <div class="row items-push">
                        <div class="col-sm-7">
                            <h1 class="page-heading">
                                @yield('page-heading')
                            </h1>                        
                        </div>
                        <div class="col-sm-5 text-right hidden-xs">
                            <ol class="breadcrumb push-10-t">
                                @yield('breadcrumb')
                            </ol>
                        </div>
                    </div>
                </div>
                <!-- END Page Header -->

                <!-- Page Content -->
                <div class="content">
                    {!! Alert::render() !!}
                    @yield('content')
                </div>
                <!-- END Page Content -->
            </main>

            @include('layouts.footer')
        </div>

        @include('layouts.modal')
    </div>
        <!-- END Apps Modal -->

        <!-- OneUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock, Appear, CountTo, Placeholder, Cookie and App.js -->
        <script src="{{ mix('js/app.js') }}"></script>
        <script src="{{ mix('js/core.js') }}"></script>
        <script>
            $(document).ready(function(){
                //$('#presupuestar').modal('toggle');
                $('#time-line-scroll').slimScroll({
                        height: 272
                });
            })
            
        </script>
    </body>
</html>