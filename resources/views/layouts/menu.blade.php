<nav id="sidebar">
    <!-- Sidebar Scroll Container -->
    <div id="sidebar-scroll">
        <!-- Sidebar Content -->
        <!-- Adding .sidebar-mini-hide to an element will hide it when the sidebar is in mini mode -->
        <div class="sidebar-content">
            <!-- Side Header -->
            <div class="side-header side-content bg-white-op">
                <!-- Layout API, functionality initialized in App() -> uiLayoutApi() -->
                <button class="btn btn-link text-gray pull-right hidden-md hidden-lg" type="button" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times"></i>
                </button>
                <a class="h5 text-white" href="index.html">
                    <img src="{{ asset('storage/logo-miyama.png') }}" class="img-responsive" alt="">
                </a>
            </div>
            <!-- END Side Header -->

            <!-- Side Content -->
            <div class="side-content">
                {!! Menu::make($menu, 'nav-main') ->setActiveClass('active') !!}
                <!--

                    <li>
                        <a href="#"><i class="si si-drawer"></i><span class="sidebar-mini-hide">Facturas</span></a>
                    </li>

                    <li>
                        <a href="#"><i class="si si-users"></i><span class="sidebar-mini-hide">Productos</span></a>
                    </li>

                    <li>
                        <a href="#"><i class="si si-users"></i><span class="sidebar-mini-hide">Solicitudes</span></a>
                    </li>

                    

                    <li>
                        <a href=""><i class="si si-users"></i><span class="sidebar-mini-hide">Empresas</span></a>
                    </li>

                    <li>
                        <a href=""><i class="si si-users"></i><span class="sidebar-mini-hide">Inventario</span></a>
                    </li>

                    <li>
                        <a href=""><i class="si si-users"></i><span class="sidebar-mini-hide">Sucursales</span></a>
                    </li>

                    <li>
                        <a href=""><i class="si si-users"></i><span class="sidebar-mini-hide">Marcas</span></a>
                    </li>
                    
                    <li>
                        <a href="{{ asset('admin/menus') }}"><i class="fa fa-bars"></i><span class="sidebar-mini-hide">Menus</span></a>
                    </li> 
                    
                    <li>
                        <a href="{{ asset('admin/medias') }}"><i class="si si-social-dropbox"></i><span class="sidebar-mini-hide">Media</span></a>
                    </li>
            -->
            </div>
            <!-- END Side Content -->
        </div>
        <!-- Sidebar Content -->
    </div>
    <!-- END Sidebar Scroll Container -->
</nav>