<ul class="{{ $class }}">
    @foreach ($items as $item)
        <li @if ($item['class']) class="{{ $item['class'] }}" @endif id="menu_{{ $item['id'] }}">
            @if (empty($item['submenu']))
                <a href="{{ $item['url'] }}" class="{{ $item['class'] }}">
                    <i class="{{ $item['icon'] }}"></i>
                    {{ $item['title'] }}
                </a>
            @else
                <a href="{{ $item['url'] }}" class="nav-submenu" data-toggle="nav-submenu">
                    <i class="{{ $item['icon'] }}"></i>                    
                    <span class="sidebar-mini-hide">{{ $item['title'] }}</span>
                </a>
                <ul>
                    @foreach ($item['submenu'] as $subitem)
                        <li>                                             
                            <a href="{{ $subitem['url'] }}"><i class="{{ $subitem['icon'] }}"></i> {{ $subitem['title'] }}</a>
                        </li>
                    @endforeach
                </ul>
            @endif
        </li>
    @endforeach
</ul>