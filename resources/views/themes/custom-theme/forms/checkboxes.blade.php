@foreach($checkboxes as $checkbox)
    <div class="checkbox">
        <label class="css-input css-checkbox css-checkbox-primary">
            {!! Form::checkbox(
                $checkbox['name'],
                $checkbox['value'],
                $checkbox['checked'],
                ['id' => $checkbox['id']]
            ) !!}
            <span></span>
            {{ $checkbox['label'] }}
        </label>
    </div>
@endforeach