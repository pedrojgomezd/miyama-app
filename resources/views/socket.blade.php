<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Socket.io</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="/css/app.css">
</head>
<body>
	
	<div id="app">
		<h2>New User</h2>

		<ul>
			<li v-for="user in users" v-text="user"></li>
		</ul>
	</div>

	<script src="/js/socket.js"></script>
	<script src="/js/vue.js"></script>

	<script>
		var socket = io('http://localhost:3000');
		vm = new Vue({
			el: '#app',
			data: {
				users: []
			},
			created: function() {
				socket.on('channel-name:UserPrueba', function (data) {
					this.users.push(data.username);
				}.bind(this))
			}
		})
	</script>

</body>
</html>