@extends('layouts.template')

@section('page-heading')
<i class="si si-users"></i> Lineas <a class="btn btn-success btn-sm" data-toggle="modal" href='#modal-id'>Agregar Lines</a>
@endsection

@section('content')

<div class="col-md-12">
	<div class="block block-themed block-rounded">
		<div class="block-header bg-primary">
			<div class="block-options-simple btn-group btn-group-xs">
				
			</div>
			<h3 class="block-title">Liena </h3>
		</div>
		<div class="">
			<table class="table">
				<thead>
					<tr>
						<th>Nombre de la Linea</th>
						<th>Sub Lines</th>
					</tr>
				</thead>
				<tbody>
				@foreach($lines as $line)
					<tr>
						<td>{{ $line->name }}</td>
						<td>
							<ul>
								<li><button onclick="$('.new-sub-line-{{$line->id}}').toggle()" class="btn btn-default"><i class="si si-plus"></i> Nueva Sub Linea</button></li>
								<li style="display: none" class="new-sub-line-{{$line->id}}">
									{{ Form::open(['method'=>'PUT','url'=> route('lines.update', ['id' => $line->id])]) }}
										{!! Field::text('name') !!}
									{{ Form::close() }}
								</li>
								@foreach($line->sub_lines as $sub_line)
									<li>{{ $sub_line->name }}</li>
								@endforeach
							</ul>
						</td>
					</tr>
				@endforeach
				</tbody>
			</table>
		</div>
	</div>

<div class="modal fade" id="modal-id">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">Nueva Linea</h4>
			</div>
			{{ Form::open(['method' => 'POST','route' => 'lines.store']) }}
			<div class="modal-body">
					{!! Field::text('name') !!}
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
				<button type="button" class="btn btn-primary">Guardar</button>
			</div>
			{{ Form::close() }}
		</div>
	</div>
</div>
@endsection