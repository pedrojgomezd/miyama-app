@extends('layouts.template')

@section('page-heading')
	<i class="si si-wrench"></i> Ordenes de Servicios
	<button class="btn btn-success" 
			data-toggle="modal" 
			data-target="#modal-filters" 
			type="button"><i class="fa fa-filter"></i> Filtrar</button>
	@if(request()->fullUrl() != request()->url())
	<a 	href="{{ request()->url() }}"
		class="btn btn-default btn-xs"> 
		Eliminar Filtros
	</a>
	@endif
@endsection

@section('content')

@include('orders.block.header-panel')

<div class="block block-themed block-rounded">
	<div class="block-header bg-primary">
		<h3 class="block-title">Lista de Ordenes</h3>
	</div>
	<div>
	<table class="table table-hover table-condensed">
		<thead>
			<tr>
				<th>#</th>
				<th>Fecha</th>
				<th>Cliente</th>
				<th>Marca</th>
				<th>Producto</th>
				<th>Estatus</th>
				<th>Tipo</th>
				<th>Accion</th>
			</tr>
		</thead>
		<tbody>
			@forelse($orders as $order)

				<tr>
					<td>{{ $order->id }}</td>
					<td>{{ $order->created_at->diffForHumans() }}</td>
					<td><a href="{{ route('clients.show', [ 'id'=> $order->client->id ]) }}">{{ $order->client->business_name }}</a></td>
					<td>{{ $order->brand->name }}</td>
					<td>{{ $order->product->name }}</td>
					<td>
						{!! $order->statu !!}
					</td>
					<td>{{ __($order->type) }}</td>
					<td>
						<div class="btn-group">
							<a href="{{ route('orders.show', ['id' => $order->id]) }}" class="btn btn-primary btn-xs">
								<i class="fa fa-eye"></i>
							</a>
							@can('revisar', $order)
							<a href="{{ route('order.statu', ['id' => $order->id, 'statu' => 'revision']) }}" class="btn btn-success btn-xs">
								Revisar
							</a>
							@endcan
						</div>
					</td>
				</tr>
			@empty

			<tr class="alert alert-danger">
				<td colspan="7">
					No seencontro registro en esta busqueda!
				</td>
			</tr>

			@endforelse
		</tbody>
	</table>
		{!! $orders->links() !!}
	</div>
</div>


{{-- Modal Filter --}}

@include('orders.block.filters-modal')

@endsection