<div class="block">
	<div class="block-content">
		{{ Form::open(['method' => 'GET']) }}
			<div class="row">
				<div class="col-md-3">
					{!! Field::select('brand', $brands,request('brand')) !!}
				</div>
				<div class="col-md-3">
					{!! Field::text('client', request('client'),['placeholder' => 'Razon Social']) !!}
				</div>
				<div class="col-md-2">
					{!! Field::select('product') !!}
				</div>
				<div class="col-md-2">
					{!! Field::select('statu') !!}
				</div>
				<div class="col-md-2">
					<br>
					<button type="submit" class="btn btn-primary">
						<i class="fa fa-search"></i>
					</button>
				</div>
			</div>
		{{ Form::close() }}
	</div>
</div>