<div class="modal fade" id="presupuestar">
	<div class="modal-dialog  modal-dialog-slideleft modal-lg">
		<div class="modal-content">
			<div class="block block-rounded block-themed block-transparent remove-margin-b">
				<div class="block-header bg-primary-dark">
					<ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
					<h3 class="block-title">
						Presupuestar
						<button class="btn btn-success btn-sm"><i class="si si-plus"></i> Nueva Partida</button>
					</h3>
				</div>
				<div>
					<table-presupuesto />
				</div>
			</div>
			<div class="modal-footer">
				<button class="btn btn-primary">Guardar</button>
			</div>
		</div>
	</div>
</div>