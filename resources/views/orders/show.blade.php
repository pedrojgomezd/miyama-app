@extends('layouts.template')

@section('page-heading')
<i class="si si-wrench"></i> Orden # {{ $order->id }}
@endsection

@section('content')

<div class="row">
	<div class="col-md-12" id="info">
		<div class="block block-rounded">
			<div class="block-header bg-gray">
				<h4 class="block-title">
					Informacion Cliente
				</h4>
			</div>
			<div>
				<table class="table table-condensed">
					<tbody>
						<tr>
							<th>Indentificacion</th>
							<th>Razon Social</th>
							<th>Telefonos</th>
							<th colspan="2">Email</th>
						</tr>
						<tr>
							<td>{{ $order->client->full_identification }}</td>
							<td>{{ $order->client->business_name }}</td>
							<td>{{ $order->client->mobil }}</td>
							<td colspan="2">{{ $order->client->email }}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="block block-rounded">
			<div class="block-header bg-gray">
				<h4 class="block-title">
					Informacion de la orden
					{!! $order->statu !!}
				</h4>
			</div>
			<div>
				<table class="table table-condensed">
					<tbody>
						<tr>
							<th>Marca</th>
							<th>Linea</th>
							<th>Sub Linea</th>
							<th>Modelo</th>
							<th>Producto</th>
						</tr>
						<tr>
							<td>{{ $order->brand->name }}</td>
							<td>{{ $order->product->sub_line->line->name }}</td>
							<td>{{ $order->product->sub_line->name }}</td>
							<td>{{ $order->model }}</td>
							<td>{{ $order->product->name }}</td>
						</tr>
						<tr>
							<th>Tipo de Orden</th>
							<th colspan="4">Direccion</th>
						</tr>
						<tr>
							<td>{{ __($order->type) }}</td>
							<td colspan="4">{!! $order->full_address !!}</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<!--Time Line Component(Migrar VUEJS-->
</div>

<div class="row">
	@include('orders.block.time-line')
	@include('orders.block.presupuesto')
</div>

@include('orders.presupuestar')

@endsection