<div class="row">
    <div class="col-sm-6 col-md-3">
        <a class="block block-link-hover3 text-center" href="javascript:void(0)">
            <div class="block-content block-content-full">
                <div class="h1 font-w700 text-primary" data-toggle="countTo" data-to="35">35</div>
            </div>
            <div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Pendientes</div>
        </a>
    </div>
    <div class="col-sm-6 col-md-3">
        <a class="block block-link-hover3 text-center" href="javascript:void(0)">
            <div class="block-content block-content-full">
                <div class="h1 font-w700" data-toggle="countTo" data-to="120">120</div>
            </div>
            <div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Hoy</div>
        </a>
    </div>
    <div class="col-sm-6 col-md-3">
        <a class="block block-link-hover3 text-center" href="javascript:void(0)">
            <div class="block-content block-content-full">
                <div class="h1 font-w700" data-toggle="countTo" data-to="260">260</div>
            </div>
            <div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Semana</div>
        </a>
    </div>
    <div class="col-sm-6 col-md-3">
        <a class="block block-link-hover3 text-center" href="javascript:void(0)">
            <div class="block-content block-content-full">
                <div class="h1 font-w700" data-toggle="countTo" data-to="57890">57890</div>
            </div>
            <div class="block-content block-content-full block-content-mini bg-gray-lighter text-muted font-w600">Mes</div>
        </a>
    </div>
</div>