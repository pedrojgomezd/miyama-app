<div class="modal fade" id="modal-filters" tabindex="-1" role="dialog" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-top">
        {{ Form::open(['method' => 'GET', 'class' => 'modal-content']) }}
            <div class="block block-themed block-transparent remove-margin-b">
                <div class="block-header bg-primary-dark">
                    <ul class="block-options">
                        <li>
                            <button data-dismiss="modal" type="button"><i class="si si-close"></i></button>
                        </li>
                    </ul>
                    <h3 class="block-title">Filtro de ordenes.</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-md-4">
                            {!! Field::text('client', request('client'), ['placeholder' => 'Razon Social']) !!}
                        </div>
                        <div class="col-md-4">
                            {!! Field::select('brand', $brands, request('brand')) !!}
                        </div>
                        <div class="col-md-4">
                            {!! Field::select('product') !!}
                        </div>
                    </div>
                <div class="row">
                    <div class="col-md-4">
                        {!! Field::select('statu', $status, request('statu')) !!}
                    </div>
                </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-sm btn-default" type="button" data-dismiss="modal">Salir</button>
                <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-check"></i> Aplicar Filtros</button>
            </div>
        {{ Form::close() }}
    </div>
</div>