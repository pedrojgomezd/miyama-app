<div class="col-md-4">
	<div class="block block-bordered block-rounded">
		<div class="block-header bg-flat">
			<h4 class="block-title">
				Linea de Tiempo
			</h4>
		</div>
		<div class=""  id="time-line-scroll">
			<ul class="list list-activity push">
			@foreach($order->status as $statu)
                <li>
                    <i class="si si-wrench text-success"></i>
                    <div class="font-w600">Cambio de Estatus a 
                        <span class='label label-info'><i class='fa fa-send'></i> {{ $statu->name }}</span>
                    </div>
                    <div>Por Pedro J Gomez</div>
                    <div><small class="text-muted">{{ $statu->pivot->created_at->diffForHumans() }}</small></div>
                </li>
            @endforeach
            </ul>
		</div>
	</div>		
</div>