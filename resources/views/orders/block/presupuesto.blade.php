<div class="col-md-6">
	<div class="block block-bordered block-rounded">
		<div class="block-header bg-city">
			<h4 class="block-title">
				Presupuesto
			</h4>
		</div>
		<div>
			<table class="table table-condensed">
				<thead>
					<tr>
						<th>Descripcion</th>
						<th>Cantidad</th>
						<th>Costo</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>LED</td>
						<td>1</td>
						<td>20000</td>
						<td>20000</td>
					</tr>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="3" class="text-right">Sub Total</th>
						<th>2000</th>
					</tr>
					<tr>
						<th colspan="3" class="text-right">IVA</th>
						<th>12%</th>
					</tr>
					<tr>
						<th colspan="3" class="text-right">Total</th>
						<th>2000</th>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>