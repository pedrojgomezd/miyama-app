@extends('layouts.template')

@section('page-heading')
<i class="si si-wrench"></i> Nueva Orden 
@endsection

@section('content')
<div class="block block-themed block-rounded">
	<div class="block-">
		<table class="table">
			<thead>
				<tr>
					<th>Identificacion</th>
					<th>Razon Social</th>
					<th>Telefono</th>
					<th>Email</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>{{ $client->full_identification }}</td>
					<td>{{ $client->business_name }}</td>
					<td>{{ $client->mobil }}</td>
					<td>{{ $client->email }}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

{{ Form::open(['mehtod'=>'POST', 'url' => route('order.generate', ['id' => $client->id]) ]) }}
<div class="block block-rounded">
	<div class="block-header bg-gray-lighter">
		<h3 class="block-title">Informes de la orden</h3>
	</div>
	<div class="block-content">
		<div class="row">
			<div class="col-md-2">
				{!! Field::select('brand_id', $brands) !!}
			</div>
			<div class="col-md-2">
				{!! Field::select('line', $lines) !!}
			</div>
			<div class="col-md-2">
				{!! Field::select('sub_line') !!}
			</div>
			<div class="col-md-2">
				{!! Field::select('product_id') !!}
			</div>
			<div class="col-md-2">
				{!! Field::text('model') !!}
			</div>
			<div class="col-md-2">
				{!! Field::text('serial') !!}
			</div>
		</div>

		<div class="row">
			<div class="col-md-4">
				{!! Field::text('fail') !!}
			</div>
			<div class="col-md-4">
				{!! Field::select('tech_id', $techs) !!}
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label for="domicilio" class="control-label"> ¿Es a domicilio?</label>
					<div class="controls">
						<label class="css-input switch switch-success">
							No
		                    <input id="domicilio" v-model="addressShow" name="domicilio" type="checkbox"><span></span>
		                    Si
		                </label>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="block" v-show="addressShow">
	<div class="block-header bg-gray-lighter">
	    <h3 class="block-title">Direccion de servicio </h3>
	</div>
	<div class="block-content">
	    <div class="row">
	        <div class="col-lg-6">
	            <!-- Billing Address -->
	            <div class="block block-bordered">
	                <div class="block-header">
	                    <h3 class="block-title">
	                       	<label class="css-input css-radio css-radio-lg css-radio-primary push-10-r">
	                            <input type="radio" name="type_address" checked="" value="{{ optional($client->addresses->first())->id }}"><span></span>
	                    		Direccion Actual
	                        </label>
	                    </h3>
	                </div>
	                <div class="block-content block-content-full">
	                    <div class="h4 push-5">{{ $client->business_name }}</div>
	                    <address>                        	
	                        {!! $client->full_address !!}<br><br>
	                        <i class="fa fa-phone"></i> {{ $client->mobil }}<br>
	                    </address>
	                </div>
	            </div>
	            <!-- END Billing Address -->
	        </div>
	        <div class="col-lg-6">
	            <!-- Shipping Address -->
	            <div class="block block-bordered">
	                <div class="block-header">
	                    <h3 class="block-title">
	                    	<label class="css-input css-radio css-radio-lg css-radio-primary push-10-r">
	                            <input type="radio" name="type_address" value="0"><span></span>
	                    		Otra direccion
	                        </label>
	                    </h3>
	                </div>
	                <div class="block-content block-content-full">
	                    <div class="row">
	                    	<div class="col-md-6">{!! Field::text('person_contact') !!}</div>
	                    	<div class="col-md-6">{!! Field::text('mobil') !!}</div>
	                    </div>
	                    
	                    <div class="row">
	                    	<div class="col-md-6">{!! Field::select('state_id', $states) !!}</div>
	                    	<div class="col-md-6">{!! Field::select('city_id') !!}</div>
	                    </div>

	                    {!! Field::textarea('address', ['rows' => 2]) !!}
	                </div>
	            </div>
	            <!-- END Shipping Address -->
	        </div>
	    </div>
	</div>
</div>

<div class="block">
	<div class="block-content">
		<div class="form-group">
			<button class="btn btn-primary pull-right"><i class="fa fa-save"></i> Guardar</button>
			<button class="btn btn-default"><i class="fa fa-arrow-left"></i> Cancelar</button>
		</div>
	</div>
</div>
{{ Form::close() }}
@endsection