$('#line').change(function(){
	clear('#sub_line');
	clear('#product_id');
    load(this.value, 'line', '#sub_line');
});

$('#sub_line').change(function(){
	clear('#product_id');
    load(this.value, 'subline', '#product_id');
});

function clear(clearable){
	$(clearable).children().remove();
}

function load(id, loadble, rellenable) {
	if (id == null) {
		return ;
	}
	axios.post('/api/'+loadble+'/'+id).then(function(response){
	        $.each(response.data, function(id, value){
			$(rellenable).append('<option value="'+id+'">'+value+'</option>');
		});
    });
}
