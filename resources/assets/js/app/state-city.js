$('#state_id').change(function(){
	clearCities();
    loadCities(this.value);
});

function clearCities(){
	$('#city_id').children().remove();
}

function loadCities(state_id) {
	if (state_id == null) {
		return ;
	}
	axios.post('/api/state', {state: state_id}).then(function(response){
	        $.each(response.data, function(id, value){
			$('#city_id').append('<option value="'+id+'">'+value+'</option>');
		});
    });
}
