<?php

use Illuminate\Http\Request;
use Miyama\Products\Line;
use Miyama\Products\SubLine;
use Miyama\Address\State;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('state', function(Request $request) {
	$state = State::find($request->state);
    $cities = $state->cities;
    $cities = $cities->pluck('name', 'id');
    return $cities->toJson();
});

Route::post('line/{line}', function(Line $line) {
    $sub_lines = $line->sub_lines;
    $sub_lines = $sub_lines->pluck('name', 'id');
    return $sub_lines->toJson();
});

Route::post('subline/{subline}', function(SubLine $subline) {
    $products = $subline->products;
    $products = $products->pluck('name', 'id');
    return $products->toJson();
});

//