<?php


Route::get('/', function () {
	return view('welcome');
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth', 'namespace' => 'Admin'], function() {
    Route::get('/', function() {
        return view('dashboard.index');
    });
	
	//CRUD Usuarios
	Route::resource('users', 'UserController');

	//Roles
	Route::resource('roles', 'RoleController');

	//Menus
	require __DIR__.'/admin/menu_route.php';

});

Route::group(['prefix' => 'reception', 'middleware' => 'auth'], function() {
    //Clientes Routes
	Route::resource('clients', 'Admin\ClientController');
	
	//Generate List and update "Orders"
	require __DIR__.'/reception/orders_route.php';

});

Route::group(['prefix' => 'settings', 'namespace' => 'Settings'], function() {
    Route::resource('lines', 'LineController');
});

//Perfil de usuario logeado
Route::get('profile', [
		'uses' => 'ProfileController@index',
		'as' => 'profile.index',
]);

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
