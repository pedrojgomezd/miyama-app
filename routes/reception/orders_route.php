<?php 

Route::group(['namespace' => 'Orders'], function() {

	Route::get('orders', [
		'uses' => 'OrdersController@index',
		'as' => 'orders.index'
	]);

	Route::get('order/{order}', [
		'uses' => 'OrdersController@show',
		'as' => 'orders.show'
	]);

	Route::get('clients/{client}/order', [
		'uses' => 'GenerateOrderController@create',
		'as' => 'order.create'
	]);

	Route::post('order/generate/{client}', [
		'uses' => 'GenerateOrderController@generate',
		'as' => 'order.generate'
	]);

	Route::get('order/statu/{order}', [
		'uses' => 'StatusOrderController',
		'as' => 'order.statu'
	]);

});
