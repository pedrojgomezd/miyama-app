<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
            [
                [
                    'name' => 'admin',
                    'label' => 'Administrator',
                ],
                [
                    'name' => 'user',
                    'label' => 'User',
                ],
                [
                    'name' => 'tech',
                    'label' => 'Techn',
                ],
                [
                    'name' => 'reception',
                    'label' => 'Reception',
                ],
            ]
        );
        DB::table('permissions')->insert(
        	[
        		[
        			'name' => 'show-order',
        			'label' => 'Ver Ordenes',
        		],
        		[
        			'name' => 'edit-order',
        			'label' => 'Editar Ordenes',
        		],
                [
                    'name' => 'create-order',
                    'label' => 'Crear Ordenes',
                ],
        		[
        			'name' => 'dele-order',
        			'label' => 'Eliminar Ordenes',
        		],
        		[
        			'name' => 'list-order',
        			'label' => 'Lista de Ordenes',
        		],
        	]
        );
    }
}
