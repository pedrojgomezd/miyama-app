<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Miyama\Admin\Client::class, 4)->create()->each( function ($c) {
            
            $c->addresses()->save(factory(Miyama\Address::class)->make());
        	
            $c->orders()->save(factory(Miyama\Order::class)->make());
            
        });
    }
}
