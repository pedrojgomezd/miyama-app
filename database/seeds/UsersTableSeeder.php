<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\User::class)->create([ 'name' => 'Pedro J Gomez', 'email' => 'pedroj.gomezd@gmail.com', 'avatar' => 'avatar.jpg', 'role_id' => 1])->each(function($u){
        	$u->addresses()->save(factory(Miyama\Address::class)->make());
        });
    }
}
