<?php

use Illuminate\Database\Seeder;

class MenusMenuItemsSeederTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->insert([
        	'name' => 'admin'
        ]);

        DB::table('menu_items')->insert([
        	[
        		'menu_id' => 1,
        		'title' => 'clients',
        		'route' => 'clients.index',
        		'icon' => 'si si-users',
        		'order' => 1,

        	],
        	[
        		'menu_id' => 1,
        		'title' => 'orders',
        		'route' => 'orders.index',
        		'icon' => 'si si-wrench',
        		'order' => 2,

        	],
        	[
        		'menu_id' => 1,
        		'title' => 'users',
        		'route' => 'users.index',
        		'icon' => 'si si-users',
        		'order' => 3,

        	],
        ]);
    }
}