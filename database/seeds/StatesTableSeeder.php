<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('states')->insert([
			['name' => 'Amazonas', 'iso_3166-2' => 'VE-X'],
			['name' => 'Anzoátegui', 'iso_3166-2' => 'VE-B'],
			['name' => 'Apure', 'iso_3166-2' => 'VE-C'],
			['name' => 'Aragua', 'iso_3166-2' => 'VE-D'],
			['name' => 'Barinas', 'iso_3166-2' => 'VE-E'],
			['name' => 'Bolívar', 'iso_3166-2' => 'VE-F'],
			['name' => 'Carabobo', 'iso_3166-2' => 'VE-G'],
			['name' => 'Cojedes', 'iso_3166-2' => 'VE-H'],
			['name' => 'Delta Amacuro', 'iso_3166-2' => 'VE-Y'],
			['name' =>  'Falcón', 'iso_3166-2' => 'VE-I'],
			['name' =>  'Guárico', 'iso_3166-2' => 'VE-J'],
			['name' =>  'Lara', 'iso_3166-2' => 'VE-K'],
			['name' =>  'Mérida', 'iso_3166-2' => 'VE-L'],
			['name' =>  'Miranda', 'iso_3166-2' => 'VE-M'],
			['name' =>  'Monagas', 'iso_3166-2' => 'VE-N'],
			['name' =>  'Nueva Esparta', 'iso_3166-2' => 'VE-O'],
			['name' =>  'Portuguesa', 'iso_3166-2' => 'VE-P'],
			['name' =>  'Sucre', 'iso_3166-2' => 'VE-R'],
			['name' =>  'Táchira', 'iso_3166-2' => 'VE-S'],
			['name' =>  'Trujillo', 'iso_3166-2' => 'VE-T'],
			['name' =>  'Vargas', 'iso_3166-2' => 'VE-W'],
			['name' =>  'Yaracuy', 'iso_3166-2' => 'VE-U'],
			['name' =>  'Zulia', 'iso_3166-2' => 'VE-V'],
			['name' =>  'Distrito Capital', 'iso_3166-2' => 'VE-A'],
			['name' =>  'Dependencias Federales', 'iso_3166-2' => 'VE-Z'],
			]);
    }
}
