<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status')->insert([
        	[
        		'name'	=> 'Ingresado',
				'slug'	=> 'ingresado',
				'icon'	=> 'si si-drawer',
				'class'	=> 'bg-info',
				'statu'	=> 'open',
			],
			[
        		'name'	=> 'Revision',
				'slug'	=> 'revision',
				'icon'	=> 'si si-magnifier',
				'class'	=> 'bg-success',
				'statu'	=> 'open',
			],
			[
        		'name'	=> 'Entregago',
				'slug'	=> 'entregado',
				'icon'	=> 'si si-like',
				'class'	=> 'bg-success',
				'statu'	=> 'close',
			],
        ]);
    }
}
