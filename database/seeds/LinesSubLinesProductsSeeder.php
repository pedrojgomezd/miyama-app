<?php

use Illuminate\Database\Seeder;

class LinesSubLinesProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Miyama\Products\Line::class, 2)->create()->each(function($l){
        	$l->sub_lines()->saveMany(factory(Miyama\Products\SubLine::class,3)->make())
                ->each(function($s)
                {
                    $s->products()->saveMany(factory(Miyama\Products\Product::class, 5)->make());
                });
        });

        factory(Miyama\Products\Brand::class, 7)->create();
    }
}
