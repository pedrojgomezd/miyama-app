<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'identification' => $faker->buildingNumber,
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'mobil' => $faker->phoneNumber,
        'type' => $faker->randomElement(['admin', 'tech', 'user']),
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Miyama\Admin\Client::class, function(Faker\Generator $faker){
    return [
        'type_identification' => $faker->randomElement(['V', 'J', 'G']),
        'identification' => $faker->numberBetween(1000000, 3000000),
        'business_name' => $faker->name,
        'email' => $faker->email,
        'mobil' => $faker->phoneNumber,
    ];
});


$factory->define(Miyama\Address::class, function(Faker\Generator $faker){
    return [
        'state_id' => 1,
        'city_id' => 1,
        'address' => $faker->address,
    ];
});

$factory->define(Miyama\Products\Line::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->randomElement(['Blanca', 'Marron']),
    ];
});

$factory->define(Miyama\Products\SubLine::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->randomElement(['Tv', 'Nevera', 'DVD', 'Aire']),
    ];
});

$factory->define(Miyama\Products\Product::class, function(Faker\Generator $faker){
    return [
        'name' => $faker->randomElement(['Tv 32', 'Splic', 'DVD']),
    ];
});

$factory->define(Miyama\Products\Brand::class, function(Faker\Generator $faker){
    $name = $faker->randomElement(['Sansung', 'LG', 'SONY']);
    return [
        'name' => $name,
        'slug' => $name,
    ];
});

$factory->define(Miyama\Order::class, function(Faker\Generator $faker){
    return [
            'user_id' => factory(App\User::class)->create()->id,
            'tech_id' => factory(App\User::class)->create()->id,
            'brand_id' => factory(Miyama\Brand::class)->id,
            'product_id' => factory(Miyama\Product::class)->id,
            'model' => $faker->sentence(1),
            'serial' => $faker->numberBetween(1000000, 3000000),
            'fail' => $faker->sentence,
        ];
});

$factory->define(Miyama\Roles\Role::class, function(Faker\Generator $faker){
    return [
            'name' => $faker->sentence,
        ];
});
