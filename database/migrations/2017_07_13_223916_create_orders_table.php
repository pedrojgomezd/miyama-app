<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_id');
            $table->integer('user_id');
            //Hara referencia a "users" pero en el modelo sera como "tech"
            $table->integer('tech_id')->nullable();
            $table->integer('brand_id');
            $table->integer('product_id');
            $table->string('serial')->nullable();
            $table->string('model');
            $table->string('fail');
            $table->enum('type', ['internal', 'delivery'])->default('internal');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
